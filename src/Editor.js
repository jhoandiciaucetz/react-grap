import React, { useEffect, useState } from "react";
import grapesjs from "grapesjs";
import gjsBlocksBasic from "grapesjs-blocks-basic";
import { useParams } from "react-router-dom";
import "./styles/main.scss"
import Sidebar from "./components/Sidebar";
import TopNav from "./components/TopNav";
import PageSection from "./components/PageSection";

const Editor = () => {
    const [editor, setEditor] = useState(null);
    const { pageId } = useParams();
    console.log('pageId :>>', pageId);
    useEffect(() => {
        const editor = grapesjs.init({
            container: "#editor",
            blockManager:{
                appendTo:"blocks",
            },
            styleManager:{
                appendTo:"#styles-container",
                sectors:[
                    {
                        name:"Dimesion",
                        open:false,
                        buildProps:["widht", "min-height","padding"],
                        properties:[{
                            type:"integer",
                            name:"The widht",
                            property:"widht",
                            units: ["px","%"],
                            defaults: "auto",
                            min:0,
                        },
                        ],
                    },
                ],
            },
            layerManager:{
//https://www.youtube.com/watch?v=p5IRWREqxr0&list=PLCl1SVi3yR4fH4r056GhRfmPE25Ntg_cy&index=19&ab_channel=CodeDexterous
            },
            plugins: [gjsBlocksBasic],
            pluginsOpts: {
                gjsBlocksBasic: {}
            },
        });
        setEditor(editor);
    }, []);

    return (
        <div className="App">
            <div
                id="navbar"
                className="sidenav d-flex flex-column overflow-scroll position-fixed"
            >
                <nav className="navbar navbar-light">
                    <div className="container-fluid">
                        <span className="navbar-brand mb-0 h3 logo">Code</span>
                    </div>
                </nav>
                <PageSection pages={pages} />
                <Sidebar />
            </div>
            <div
                className="main-content position-relative w-85 start-15"
                id="main-content"
            >
                <TopNav />
                <div id="editor"></div>
            </div>
        </div>

    )
};

export default Editor;