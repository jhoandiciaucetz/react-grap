import React from "react";
import "./styles/main.scss";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Editor from "./Editor";
import Home from "./Home";
function App() {
  return (
    <Router>
      <Routes>
        <Route exac path="/" Component={Home} />
        <Route exac path="/editor/:pageId" Component={Editor} />
      </Routes>
    </Router>
  );
}

export default App;
